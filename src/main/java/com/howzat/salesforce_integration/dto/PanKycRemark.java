package com.howzat.salesforce_integration.dto;

public enum PanKycRemark {
	DOC_NOT_SUBMITTED(0),
	DOC_SUBMITTED(1),
	BLOCKED_STATE(2),
	BELOW_18(3),
	UNCLEARED_DOC(4),
	MISMATCHED_DOC(5),
	INCOMPLETED_DOC(6),
	PAN_EXISTED(7),
	DEFAULT_MSG(8),
	VERIFIED(9),
	MAX_ATTEMPTS(10),
	PAN_REQUIRED(11);

	private final int remark;

	PanKycRemark(final int newValue) {
		remark = newValue;
	}

	public int getRemark() {
		return remark;
	}

	public static PanKycRemark byid(int id) {
		for (PanKycRemark type : values()) {
			if (type.getRemark() == id) {
				return type;
			}
		}
		return null;
	}
}
