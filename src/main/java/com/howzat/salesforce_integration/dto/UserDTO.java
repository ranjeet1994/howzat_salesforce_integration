package com.howzat.salesforce_integration.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

@Data
@JsonDeserialize
public class UserDTO {

	private Long id;

	@JsonProperty("login_name")
	private String username; // should make it case insensitive

	@JsonIgnore
	private String password;

	private String email;

	private String mobile;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private LocalDate dob;

	private Gender gender;

	@JsonProperty("lang_id")
	private int langId;

	@JsonProperty("channel_id")
	private int channelId;

	private AccountStatus status = AccountStatus.ACTIVE;

	@CreatedDate
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdDate = ZonedDateTime.now();

	private UserAddressDetailsDTO address;

	private UserBankDetailsDTO bank;

	private UserPANDetailsDTO pan;

	private UserUpiDetailsDTO upi;

	private UserAuthorizationsDTO authorizations;

	private Set<SocialUserConnectionDTO> socialConnections;

	private CashAccountDTO cashAccount;

	private FunAccountDTO funAccount;

	private boolean isNewUser;

	private boolean otpSignUp;

}