package com.howzat.salesforce_integration.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class UserUpiDetailsDTO {

	private Long id;

//	private UserDTO user;

	private Integer status;

	private String vpa;

	private String agent;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdAt = ZonedDateTime.now();

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime updatedAt = ZonedDateTime.now();

}
