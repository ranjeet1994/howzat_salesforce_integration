package com.howzat.salesforce_integration.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserAuthorizationsDTO {

	private Long id;

	@JsonProperty("email_verification")
	private boolean emailVerified;

	@JsonProperty("mobile_verification")
	private boolean mobileVerified;

	@JsonProperty("pan_verification")
	private VerificationStatus panVerificationStatus = VerificationStatus.DOC_NOT_SUBMITTED;

	@JsonProperty("address_verification")
	private VerificationStatus addressVerificationStatus = VerificationStatus.DOC_NOT_SUBMITTED;

	@JsonProperty("pan_remark")
	private PanKycRemark panRemark = PanKycRemark.DOC_NOT_SUBMITTED;

	@JsonProperty("kyc_remark")
	private PanKycRemark kycRemark = PanKycRemark.DOC_NOT_SUBMITTED;

	@JsonProperty("kyc_attempts")
	private Integer kycAttempts = 0;

	@JsonProperty("pan_attempts")
	private Integer panAttempts = 0;

	@JsonBackReference
	private UserDTO user;

	public Integer getKycAttempts() {
		if (kycAttempts == null)
			kycAttempts = 0;
		return kycAttempts;
	}

	public Integer getPanAttempts() {
		if (panAttempts == null)
			panAttempts = 0;
		return panAttempts;
	}

}