package com.howzat.salesforce_integration.dto;

public enum VerificationStatus {

	DOC_NOT_SUBMITTED,

	DOC_SUBMITTED,

	UNDER_REVIEW,

	DOC_REJECTED,

	VERIFIED,

	MAX_ATTEMPTS,

	PAN_REQUIRED

}
