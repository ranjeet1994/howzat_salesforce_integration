package com.howzat.salesforce_integration.dto;

public enum AccountStatus {

	ACTIVE(1),

	CLOSED(2),

	BLOCKED(3);

	private final int status;

	private AccountStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public static AccountStatus getStatus(int status) {
		for (AccountStatus s : values()) {
			if (s.getStatus() == status)
				return s;
		}
		return null;
	}

}
