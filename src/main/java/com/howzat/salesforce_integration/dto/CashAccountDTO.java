package com.howzat.salesforce_integration.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class CashAccountDTO {

	private Long id;

//	private UserDTO user;

	private Double withdrawable;

	private Double depositBucket;

	private Double nonWithdrawable;

	private Double nonPlayableBucket;

	private double referralAmount;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdDate = ZonedDateTime.now();

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime modifiedDate = ZonedDateTime.now();

}