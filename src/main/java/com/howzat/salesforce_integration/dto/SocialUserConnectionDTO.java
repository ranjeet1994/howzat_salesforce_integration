package com.howzat.salesforce_integration.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class SocialUserConnectionDTO {

	private Long id;

	private String providerId;

	private String providerUserId;

	private String displayName;

	private String profileURL;

	private String imageURL;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private AgeRange ageRange;

	private String accessToken;

	@JsonIgnore
	private String secret;

	private String refreshToken;

	private Long expireTime;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdDate = ZonedDateTime.now();

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime modifiedDate = ZonedDateTime.now();

	private UserDTO user;

}