package com.howzat.salesforce_integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserAddressDetailsDTO {

	private Long id;

	@JsonProperty("first_name")
	private String firstname;

	@JsonProperty("middle_name")
	private String middlename;

	@JsonProperty("last_name")
	private String lastname;

	@JsonProperty("add_line_1")
	private String addressLine1;

	@JsonProperty("add_line_2")
	private String addressLine2;

	private String city;

	private String state;

	private Integer pincode;

//	private UserDTO user;
}