package com.howzat.salesforce_integration.dto;

public enum AgeRange {

	AGE_13_17(13, 17),
	AGE_18_20(18, 20),
	AGE_21_PLUS(21, null);

	private Integer min;
	private Integer max;

	private AgeRange(Integer min, Integer max) {
		this.min = min;
		this.max = max;
	}

	public Integer getMin() {
		return min;
	}

	public Integer getMax() {
		return max;
	}
}
