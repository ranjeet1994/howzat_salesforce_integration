package com.howzat.salesforce_integration.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserPANDetailsDTO {

	private Long id;

	@JsonProperty("pan_no")
	private String panNumber;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdDate = ZonedDateTime.now();

//	private UserDTO user;

}