package com.howzat.salesforce_integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserBankDetailsDTO {

	private Long id;

	@JsonProperty("account_number")
	private String bankAccountNumber;

	@JsonProperty("ifsc_code")
	private String ifscCode;

	@JsonProperty("bank_name")
	private String bankName;

	@JsonProperty("bank_branch")
	private String bankBranch;

	private UserDTO user;

}