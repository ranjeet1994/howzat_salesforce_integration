package com.howzat.salesforce_integration.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class FunAccountDTO {

	private Long id;

//	private UserDTO user;

	private Double balance;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime createdDate = ZonedDateTime.now();

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ZonedDateTime modifiedDate = ZonedDateTime.now();

}