package com.howzat.salesforce_integration.dto;

public enum Gender {

	MALE("M", "male"),

	FEMALE("F", "female");

	private final String charCode;
	private final String stringCode;

	Gender(String charCode, String stringCode) {
		this.charCode = charCode;
		this.stringCode = stringCode;
	}

	public String getCharCode() {
		return charCode;
	}

	public String getStringCode() {
		return stringCode;
	}

	public static Gender getByCharcode(String charCode) {
		if (charCode != null) {
			for (Gender g : Gender.values()) {
				if (g.charCode.equalsIgnoreCase(charCode) || g.toString().equalsIgnoreCase(charCode))
					return g;
			}
		}
		return null;
	}

	public static Gender getByStringCode(String stringCode) {
		if (stringCode != null) {
			for (Gender g : Gender.values()) {
				if (g.stringCode.equalsIgnoreCase(stringCode))
					return g;
			}
		}
		return null;
	}

}
