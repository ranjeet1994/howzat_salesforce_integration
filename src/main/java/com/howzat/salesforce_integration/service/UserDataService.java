package com.howzat.salesforce_integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.howzat.salesforce_integration.dao.UserDAO;
import com.howzat.salesforce_integration.dto.UserDTO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserDataService {
	@Autowired
	private UserDAO userDAO;

	public UserDTO findById(Long userId) {

		return fetchUserFromUPS(userId);
	}

	public UserDTO findByUserName(String userName) {
		log.info("Fetching user-data for username: {}", userName);
		Long userId = userDAO.getUserIdByUserName(userName);
		if (userId == null) {
			log.warn("username: {} doesn't exit in DB", userName);
			return null;
		}
		log.info("username: {} --> userId: {} ", userName, userId);
		return fetchUserFromUPS(userId);
	}

	public UserDTO findByMobile(String mobile) {
		log.info("Fetching user-data for mobile: {}", mobile);
		Long userId = userDAO.getUserIdByMobile(mobile);
		if (userId == null) {
			log.warn("mobile: {} doesn't exit in DB", mobile);
			return null;
		}
		log.info("mobile: {} --> userId: {} ", mobile, userId);
		return fetchUserFromUPS(userId);
	}

	private UserDTO fetchUserFromUPS(Long userId) {
		RestTemplate restTemplate = new RestTemplate();
//		String url = "http://localhost:8080/ups/api/userprofileservice/v2/users/".concat(userId.toString());
		String url = "http://ups.pfantasy.com/ups/api/userprofileservice/v2/users/".concat(userId.toString());

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("admin", "secret"));

		UserDTO response = restTemplate.getForObject(url, UserDTO.class);
		return response;
	}

}
