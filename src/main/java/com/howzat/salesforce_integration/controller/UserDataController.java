package com.howzat.salesforce_integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.howzat.salesforce_integration.dto.UserDTO;
import com.howzat.salesforce_integration.service.UserDataService;

@RestController
@RequestMapping("/si/api/userData")
public class UserDataController {
	@Autowired
	UserDataService userDataService;

	@PostMapping(value = "/findById", consumes = "application/json")
	public ResponseEntity<UserDTO> findById(@RequestParam("userid") Long userid) {
		UserDTO user = userDataService.findById(userid);
		if (user == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(user);
	}

	@PostMapping(value = "/findByUserName", consumes = "application/json")
	public ResponseEntity<UserDTO> findByUserName(@RequestParam("userName") String userName) {
		UserDTO user = userDataService.findByUserName(userName);
		if (user == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(user);
	}

	@PostMapping(value = "/findByMobile", consumes = "application/json")
	public ResponseEntity<UserDTO> findByMobile(@RequestParam("mobile") String mobile) {
		UserDTO user = userDataService.findByMobile(mobile);
		if (user == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(user);
	}
}