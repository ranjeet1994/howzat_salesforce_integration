package com.howzat.salesforce_integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesforceIntegrationApplication {
	public static void main(String[] args) {
		SpringApplication.run(SalesforceIntegrationApplication.class, args);
	}
}
