package com.howzat.salesforce_integration.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDAO {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Transactional(readOnly = true)
	public Long getUserIdByUserName(String userName) {
		log.info("Querying DB for getting userId from username: {}", userName);
		String sql = "select id from users where user_name =:userName";

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("userName", userName);

		Long userId = null;
		try {
			userId = jdbcTemplate.queryForObject(sql, paramMap, Long.class);
		} catch (DataAccessException e) {
			log.error("Query to get userId for userName: {} failed!", userName);
//			e.printStackTrace();
		}

		return userId;
	}

	public Long getUserIdByMobile(String mobile) {
		log.info("Querying DB for getting userId from mobile: {}", mobile);
		String sql = "select id from users where mobile =:mobile";

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("mobile", mobile);

		Long userId = null;
		try {
			userId = jdbcTemplate.queryForObject(sql, paramMap, Long.class);
		} catch (DataAccessException e) {
			log.error("Query to get userId for mobile: {} failed!", mobile);
//			e.printStackTrace();
		}

		return userId;
	}

}
